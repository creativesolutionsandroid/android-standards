package com.example.androidstandards;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.List;

public class ShareUsingGmail extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("plain/text");
            i.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@samkra.com"});
            i.putExtra(Intent.EXTRA_SUBJECT, "Samkra Feedback");
            i.putExtra(Intent.EXTRA_TITLE, "Samkra Feedback");
            final PackageManager pm = getPackageManager();
            final List<ResolveInfo> matches = pm.queryIntentActivities(i, 0);
            String className = null;
            for (final ResolveInfo info : matches) {
                if (info.activityInfo.packageName.equals("com.google.android.gm")) {
                    className = info.activityInfo.name;

                    if (className != null && !className.isEmpty()) {
                        break;
                    }
                }
            }
            i.setClassName("com.google.android.gm", className);
            try {
                startActivity(Intent.createChooser(i, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "There are no email apps installed.", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "There are no email apps installed.", Toast.LENGTH_SHORT).show();
        }
    }
}